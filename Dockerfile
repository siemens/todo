FROM node:10-alpine as client

WORKDIR /opt/todo

COPY package.json yarn.lock ./
RUN yarn

COPY . .

RUN yarn build:prod && \
    cd server && yarn

FROM node:10-alpine

WORKDIR /opt/todo
COPY server/package.json server/yarn.lock ./
COPY --from=client /opt/todo/server/node_modules node_modules
COPY --from=client /opt/todo/server/static static

ENTRYPOINT ["node"]
CMD ["server.js"]
