/*
Copyright Siemens AG 2018
SPDX-License-Identifier: MIT
*/

const http = require('http');
const express = require('express');
const bodyparser = require('body-parser');
const swaggerUi = require('swagger-ui-express');

const mongoose = require('mongoose');
const TodoModel = require('./model');

const TodoServer = function() {
  const setupDb = () => {
    let mongoUrl = process.env.MONGODB_URL || 'mongodb://127.0.0.1:27017/todo';

    mongoose.connect(mongoUrl, { useNewUrlParser: true })
      .then(()=> {
        console.log('Connected to ' + mongoUrl);
      })
      .catch(()=> {
        console.error('Error Connecting to ' + mongoUrl);
        process.exit(1);
      });
  };

  const setupWebserver = (app) => {
    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({ extended: false }));
  };

  const setupEndpoints = (app) => {
    // Static mappings
    app.use(express.static(__dirname + '/static/'));

    const YAML = require('yamljs');
    const swaggerDocument = YAML.load('./specs/api.yaml');
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


    app.post('/api/v1/todo', (req, res) => {
      TodoModel.create(req.body)
        .then(
          (success) => {
            res.sendStatus(201);
          },
          (error) => {
            res.sendStatus(400);
          });
    });

    app.get('/api/v1/todo', (req, res) => {
      TodoModel.find()
        .then(
          (tasks) => {
            res.json(tasks);
          },
          (error) => {
            res.sendStatus(400);
          });
    });

    app.delete('/api/v1/todo/:id', (req, res) => {
      TodoModel.deleteOne(
        {
          _id: mongoose.Types.ObjectId(req.params.id),
        })
        .then(
          (success) => {
            res.sendStatus(200);
          },
          (error) => {
            res.sendStatus(400);
          });
    });

    app.get('/api/health_check', (req, res) => {
      if (mongoose.connection.readyState === 1) {
        res.sendStatus(200);
      }
      else {
        res.sendStatus(503);
      }
    });

    // Send all other request to the angular app
    app.get('*', (req, res) => {
      res.sendFile(__dirname + '/static/');
    });
  };

  this.run = () => {
    const app = express();
    const server = http.createServer(app);

    setupDb();
    setupWebserver(app);
    setupEndpoints(app);

    server.listen(process.env.PORT || 3000, function() {
      console.log('Listening on port', server.address().port);
    });
  }
};

const todoServer = new TodoServer();
todoServer.run();
