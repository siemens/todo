# Todo

This is a simple todo app using the MEAN (MongoDB, Express.js, Angular, Node.js)
stack. It is a simplified version of the [MindSphere DevOps Demo](https://opensource.mindsphere.io/docs/devops-demo/).

## Todo app

The todo app provides examples on CI/CD including unit and e2e tests.

### Local Development

Start the local todo backend and Angular dev server. You will be able
to enjoy live reload of changes done in the source code of the Angular
app:

```sh
# Start mongodb server
docker run -p 27017:27017 mongo

# Start nodejs backend
yarn --cwd server
yarn --cwd server start

# Start Angular dev server
yarn
yarn start
```

Now load `http://localhost:4200`

You can also reach the API navigator under `http://localhost:3000/api-docs`

### Live development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app
will automatically reload if you change any of the source files.

### Live development server (with todo api server)

1. Run the todo api server available on the `server/` directory. This will
    start the api server on `http://localhost:3000`
1. Run `yarn start`. Navigate to `http://localhost:4200/`. The app will proxy
    api calls to `http://localhost:3000` and automatically reload if you change
    any of the source files

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can
also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build static Angular UI

Run `ng build` to build the project. The build artifacts will be stored in the
`dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## License

This project is licensed under the MIT License
