/*
Copyright Siemens AG 2018
SPDX-License-Identifier: MIT
*/

import { NgModule, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import * as Sentry from '@sentry/browser';
import { ErrorHandler } from '@angular/core';

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

@NgModule({
  providers: [{ provide: ErrorHandler, useClass: SentryErrorHandler }],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    AppComponent,
    TodosComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
